package AES;
import com.google.common.base.Splitter;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;




public class Gui extends masmt2.agent.MaSMTAgent implements ActionListener {

	/**
	 * 
	 */
	//private static final long serialVersionUID = 7736410446827295453L;
	private JFrame frame;
	JLabel lblwords;
	JLabel labelSent;
	JLabel labelAvg;
	JTextArea textArea;
	private JButton btnNewButtonsyno;
	//static StaticForGui stat=new StaticForGui();
	
	private JLabel lblNoOfWords;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JLabel labelMarks;
	private static JLabel labelMarks1; 
	JScrollPane sp;
	JScrollPane sp1;
	//private JButton btnUniq;
	
	public static String [] bc;
	private static JTextArea textArea_1;
	public String ab;
	//public static String cntAgt;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenu mnSet;
	private JMenuItem mntmWordLimit;
	private JMenuItem mntmExit;
	public static int minval=0;
	public static int maxval=0;
	static int solHandle=0;
	static float marks=0;
	static int coun,sc,scnt,counter,minValue=0,maxValue=100;
	static boolean subj=false;
	private Highlighter.HighlightPainter redPainter;
	private Highlighter.HighlightPainter greenPainter;
	public static JProgressBar progressBar;
	Thread t;
	static Gui a =new Gui();
	MaSMTMessage tempmes;
	int wl;
	private JMenu mnSettings;
	private JMenu mnHelp;
	private JMenuItem mntmAddValues;
	private JMenuItem mntmAbout;
	StatDetail st =new StatDetail();
	boolean agt=true;
	private String prob = null,sol=null,meth=null,res=null;
	private static String abstractt;
	
	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {

		
	/*	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Gui window = new Gui();
					
					a.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});*/
	//	bc=args;
		//stat.textRead();
		//st.splitSentense();
		
	//}

	/**
	 * Create the application.
	 * @wbp.parser.entryPoint
	 */
	public Gui() {
		super();
		initialize();
		//frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
	
	public Gui(String name,int id,String gp) {
		super(gp,name,id);
        
	}
	
	public void gstart(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}



	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		
		
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(51, 255, 102));
		frame.getContentPane().setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 11));
		frame.setBounds(100, 100, 1050, 634);
		frame.setContentPane(new JLabel(new ImageIcon("grey-website-background.jpg")));

		progressBar = new JProgressBar();
	    progressBar.setMinimum(minValue);
	    progressBar.setMaximum(maxValue);
	    progressBar.setStringPainted(true);

        progressBar.setBounds(42, 540, 980, 20);
        frame.getContentPane().add(progressBar);
		frame.getContentPane().add(progressBar);
		
		menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mnSet = new JMenu("Set");
		mnFile.add(mnSet);
		
		mntmWordLimit = new JMenuItem("Word limit");
		mntmWordLimit.addActionListener(this);
		mnSet.add(mntmWordLimit);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(this);
		mnFile.add(mntmExit);
		
		mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		mntmAddValues = new JMenuItem("Add values");
		mntmAddValues.addActionListener(this);
		mnSettings.add(mntmAddValues);
		
		mnHelp = new JMenu("help");
		menuBar.add(mnHelp);
		
		mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);
		//frame.setContentPane(new JLabel(new ImageIcon("grey-website-background.jpg")));
		frame.setTitle("Abstract Evaluation System");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		btnNewButtonsyno = new JButton("Clear");
		btnNewButtonsyno.addActionListener(this);

		btnNewButtonsyno.setForeground(new Color(204, 255, 51));
		btnNewButtonsyno.setBackground(new Color(0, 0, 51));
		btnNewButtonsyno.setFont(new Font("Georgia", Font.BOLD, 11));
		
		JButton btnNewButtonSplit = new JButton("Evaluate");
		btnNewButtonSplit.setForeground(new Color(204, 255, 51));
		btnNewButtonSplit.setBackground(new Color(0, 0, 51));
		btnNewButtonSplit.setFont(new Font("Georgia", Font.BOLD, 11));

		btnNewButtonSplit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
				
				Thread runner = new Thread() {
			          public void run() {
			            counter = minValue;
			            try {
				            //GET XML FILE NAME
				            Document doc;
				            Element root;
				            List<Element> elements;
				            // Information about English morphologycal rules
				            counter=25;
				            
				            Runnable runme = new Runnable() {
				                   public void run() {
				                     progressBar.setValue(counter);
				                   }
				                 };
				            SwingUtilities.invokeLater(runme);
				            String xmlFileName = "Values.xml";
				         //   System.out.println(">>" +xmlFileName);
				            File xmlFile = new File(xmlFileName);
				            if (xmlFile.exists()) 
				            {
				                FileInputStream fis = new FileInputStream(xmlFile);
				                SAXBuilder sb = new SAXBuilder();
				                doc = sb.build(fis);
				                root = doc.getRootElement();
				                fis.close();
				                elements = root.getChildren("Val");
				                //staff.getAttribute("id").setValue("2");
				                Element tmpNode;
				                
				                for (int m = 0; m < elements.size(); m++) {

				                    tmpNode = elements.get(m);                 
				                    maxval = Integer.parseInt(tmpNode.getAttributeValue("maxVal"));
				                    minval = Integer.parseInt(tmpNode.getAttributeValue("minVal"));
				                    
				                }
				            } 
				            else 
				            {
				                System.out.println("ERR:...");
				                JOptionPane.showMessageDialog(frame, "Please insert Values.xml");
					            System.exit(0);
				                // File Not Avialable
				            }
				        } catch (IOException io) {
				            System.out.println("XML_ERR" + io.getMessage());
				        } catch (JDOMParseException io) {
				            System.out.println("XML_ERR" + io.getMessage());
				        } catch (JDOMException e) {
				            System.out.println("XML_ERR" + e.getMessage());
				        }
						
						lblwords.setText("");
						labelSent.setText("");
						labelAvg.setText("");
						
						st.setAb("");

						
						if(minval!=0 && maxval!=0){
							
							ab=textArea.getText();
							abstractt =ab;
							System.out.println(abstractt);
							ab=ab.replaceAll("\n", " ");
							st.setAb(ab);
							textArea.setText("");
							textArea_1.setText("");
							for(final String token :
							    Splitter
							        .fixedLength(55)
							        .split(ab)){
								textArea.append(token+"\n");
								textArea_1.append(token+"\n");
							}
							//st.everything = ab;
							st.splitSentense();
							counter=40;						
							
							
							
							
							lblNoOfWords.setText("No of words in abstract");
							lblNewLabel.setText("No of sentences in abstract");
							lblNewLabel_1.setText("Avarage words of a sentence");
							st.count=0;
							coun =st.wordcount();
							lblwords.setText(Integer.toString(coun));
							//System.out.println("word cnt "+st.wordcount());
							int newlines =st.getNoOfCharactors()/55+1;
							
							//chk
							SetWordLimit limit =new SetWordLimit();
							limit.checkLimit(newlines,coun,textArea_1,a,st,minval,maxval);
							
							//lblwords.setText(Integer.toString(st.wordcount()));
							labelSent.setText(Integer.toString(st.count1));
							labelAvg.setText(Integer.toString(st.avg()));
							
							GrammarChecker grammarcheck =new GrammarChecker();
							textArea_1.append("\n------------------------------------------------------------------------------------"
									+ "\nGrammar, Spelling errors");
							grammarcheck.grammar(ab, grammarcheck,textArea_1,a);
							
							if(grammarcheck.getError()==null){
								textArea_1.append("\nNo grammar errors");
							}
							Runnable runme = new Runnable() {
				                   public void run() {
				                     progressBar.setValue(counter);
				                   }
				                 };
				            SwingUtilities.invokeLater(runme);
							MultipleWords problem =new MultipleWords();
							problem.wordusing(ab,textArea_1,st);
							labelMarks.setText("Final marks ");
							//sma.start();
							counter=60;
							textArea_1.append("---------------------------------------------------------------------------------\nTips to improve the abstract\n\n");
						}
						
			            Runnable runme = new Runnable() {
			                public void run() {
			                	try{
			                		ManagerAgent sma = new ManagerAgent();
					                sma.start();
					                progressBar.setValue(counter);
			                	}catch(Exception e){
			                		JOptionPane.showMessageDialog(frame, "Please close the application and run again");
			                		System.out.println("Error");
			                	}
			                }
			              };
			              SwingUtilities.invokeLater(runme);
			          }
			        };
			        runner.start();
				
			}
		});
		btnNewButtonsyno.setBounds(450, 510, 148, 23);
		frame.getContentPane().add(btnNewButtonsyno);
		
		btnNewButtonSplit.setBounds(50, 510, 148, 23);
		frame.getContentPane().add(btnNewButtonSplit);
		
		JLabel lblinput = new JLabel("Input text(Abstract)");
		lblinput.setForeground(new Color(204, 153, 0));
		lblinput.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 15));
		lblinput.setBounds(50, 75, 150, 19);
		frame.getContentPane().add(lblinput);
		
		JLabel result = new JLabel("Result");
		result.setForeground(new Color(204, 153, 0));
		result.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 15));
		result.setBounds(460, 75, 150, 19);
		frame.getContentPane().add(result);
		
		JLabel summary = new JLabel("Summary");
		summary.setForeground(new Color(204, 153, 0));
		summary.setFont(new Font("Microsoft PhagsPa", Font.BOLD, 15));
		summary.setBounds(855, 75, 150, 19);
		frame.getContentPane().add(summary);
		
		lblwords = new JLabel();
		lblwords.setForeground(new Color(204, 153, 0));
		lblwords.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		lblwords.setBounds(870, 167, 138, 19);
		frame.getContentPane().add(lblwords);
		
		labelSent = new JLabel();
		labelSent.setForeground(new Color(204, 153, 0));
		labelSent.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		labelSent.setBounds(870, 234, 138, 19);
		frame.getContentPane().add(labelSent);
		
		labelAvg = new JLabel();
		labelAvg.setForeground(new Color(204, 153, 0));
		labelAvg.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		labelAvg.setBounds(870, 302, 138, 19);
		frame.getContentPane().add(labelAvg);
		
		labelMarks = new JLabel();
		labelMarks.setForeground(new Color(153, 153, 204));
		labelMarks.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		labelMarks.setBounds(865, 347, 138, 19);
		frame.getContentPane().add(labelMarks);
		
		labelMarks1 = new JLabel();
		labelMarks1.setForeground(Color.green);
		labelMarks1.setFont(new Font("Segoe UI Symbol", Font.BOLD, 11));
		labelMarks1.setBounds(870, 362, 138, 19);
		frame.getContentPane().add(labelMarks1);
		
		
		
		textArea = new JTextArea();
		textArea.setBackground(new Color(204, 204, 204));
		textArea.setBounds(42, 189, 493, 195);
		Border border = BorderFactory.createLineBorder(Color.WHITE);
		textArea.setBorder(BorderFactory.createCompoundBorder(border, 
		            BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		
		textArea.setText("");
		//frame.getContentPane().add(textArea);
		
		sp =new JScrollPane(textArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp.setBounds(42, 110, 390, 379);
		frame.getContentPane().add(sp);
		
		
		
		
		
		lblNoOfWords = new JLabel();
		lblNoOfWords.setForeground(new Color(153, 153, 204));
		lblNoOfWords.setBounds(865, 148, 138, 19);
		frame.getContentPane().add(lblNoOfWords);
		
		

		
		lblNewLabel = new JLabel();
		lblNewLabel.setForeground(new Color(153, 153, 204));
		lblNewLabel.setBounds(865, 215, 180, 19);
		frame.getContentPane().add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel();
		lblNewLabel_1.setForeground(new Color(153, 153, 204));
		lblNewLabel_1.setBounds(865, 286, 180, 19);
		frame.getContentPane().add(lblNewLabel_1);
		
		JButton btnUniq = new JButton("Grammar errors");
		btnUniq.setForeground(new Color(204, 255, 0));
		btnUniq.setBackground(new Color(51, 0, 0));
		btnUniq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
		            //GET XML FILE NAME
		            Document doc;
		            Element root;
		            List<Element> elements;
		            // Information about English morphologycal rules
		    
		            String xmlFileName = "Values.xml";
		         //   System.out.println(">>" +xmlFileName);
		            File xmlFile = new File(xmlFileName);
		            if (xmlFile.exists()) 
		            {
		                FileInputStream fis = new FileInputStream(xmlFile);
		                SAXBuilder sb = new SAXBuilder();
		                doc = sb.build(fis);
		                root = doc.getRootElement();
		                fis.close();
		                elements = root.getChildren("Val");
		                //staff.getAttribute("id").setValue("2");
		                Element tmpNode;
		                String name;
		                int qun;
		                
		                for (int i = 0; i < elements.size(); i++) {

		                    tmpNode = elements.get(i);                 
		                    name = tmpNode.getAttributeValue("maxVal");
		                    qun = Integer.parseInt(tmpNode.getAttributeValue("minVal"));
		                    //syn.askSynoNoun(name.trim());
		                    System.out.println(name+" "+qun); 
		                    
		                }
		                //System.out.println("Problem count "+probCnt);
		                

		            } 
		            else 
		            {
		                System.out.println("ERR:...");
		                // File Not Avialable
		            }
		        } catch (IOException io) {
		            System.out.println("XML_ERR" + io.getMessage());
		        } catch (JDOMParseException io) {
		            System.out.println("XML_ERR" + io.getMessage());
		        } catch (JDOMException e) {
		            System.out.println("XML_ERR" + e.getMessage());
		        }
				
				lblwords.setText("");
				labelSent.setText("");
				labelAvg.setText("");
				StatDetail st =new StatDetail();
				st.setAb("");
	
					//st.everything = ("");
					//a.start();
					
					ab=textArea.getText();
					st.setAb(ab);
					textArea.setText("");
					textArea_1.setText("");
					for(final String token :
					    Splitter
					        .fixedLength(55)
					        .split(ab)){
						textArea.append(token+"\n");
						textArea_1.append(token+"\n");
					}
					//st.everything = ab;
					st.splitSentense();
					
					int cnt=st.count1;
					System.out.println(cnt);		

					lblNoOfWords.setText("No of words in abstract");
					lblNewLabel.setText("No of sentences in abstract");
					lblNewLabel_1.setText("Avarage words of a sentence");
					st.count=0;
					int coun =st.wordcount();
					lblwords.setText(Integer.toString(coun));
					//System.out.println("word cnt "+st.wordcount());
			

					labelSent.setText(Integer.toString(cnt));
					labelAvg.setText(Integer.toString(st.avg()));
					
					GrammarChecker grammarcheck =new GrammarChecker();
					textArea_1.append("\n------------------------------------------------------------------------------------"
							+ "\nGrammar errors");
					grammarcheck.grammar(ab, grammarcheck,textArea_1,a);
					
					if(grammarcheck.getError()==null){
						textArea_1.append("\nNo grammar errors");
					}
					textArea_1.append("\n\n");
					//labelMarks1.setText(Integer.toString(marks)); 
					progressBar.setValue(100);
			}
		});
		btnUniq.setFont(new Font("Georgia", Font.BOLD, 11));
		btnUniq.setBounds(250, 510, 148, 23);
		frame.getContentPane().add(btnUniq);
		
		JButton btnSvo = new JButton("SVO of sentence");
		btnSvo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SVOextracter a =new SVOextracter();
				List<String> res;
				ab=textArea.getText();
				progressBar.setValue(50);
				res=a.absList(ab);
				textArea_1.append("Result will be shown as SUBJECT-VERB-OBJECT\n\n");
				textArea.setText("");
				HighLightTextGreen(0, 43, textArea_1);
				for(final String token :
				    Splitter
				        .fixedLength(55)
				        .split(ab)){
					textArea.append(token+"\n");
				}
				for(String obj: res){
					textArea_1.append(obj+"\n");
				}
				progressBar.setValue(100);
			}
		});
		btnSvo.setForeground(new Color(204, 255, 0));
		btnSvo.setBackground(new Color(51, 0, 0));
		
		btnSvo.setFont(new Font("Georgia", Font.BOLD, 11));
		btnSvo.setBounds(650, 510, 148, 23);
		frame.getContentPane().add(btnSvo);
		
		JButton btnprint = new JButton("Print");
		btnprint.setForeground(new Color(204, 255, 0));
		btnprint.setBackground(new Color(51, 0, 0));
		btnprint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Desktop.isDesktopSupported()) {
				    try {
				        File myFile = new File("FirstPdf.pdf");
				        Desktop.getDesktop().open(myFile);
				    } catch (IOException ex) {
				        // no application registered for PDFs
				    }
				}
			}
		});
		btnprint.setFont(new Font("Georgia", Font.BOLD, 11));
		btnprint.setBounds(870, 392, 120, 19);
		frame.getContentPane().add(btnprint);

		
		textArea_1 = new JTextArea();
		textArea_1.setBackground(new Color(204, 204, 204));
		textArea_1.setBounds(570, 60, 307, 379);
		Border border1 = BorderFactory.createLineBorder(Color.WHITE);
		textArea_1.setBorder(BorderFactory.createCompoundBorder(border1, 
	            BorderFactory.createEmptyBorder(20, 20, 20, 20)));
		//frame.getContentPane().add(textArea_1);

		
		sp1 =new JScrollPane(textArea_1,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp1.setBounds(450, 110, 390, 379);
		frame.getContentPane().add(sp1);
		
		
		
		JLabel lblAbstractEvaluationSystem = new JLabel("Abstract Evaluation System ");
		lblAbstractEvaluationSystem.setForeground(Color.ORANGE);
		lblAbstractEvaluationSystem.setFont(new Font("Tempus Sans ITC", Font.BOLD, 35));
		lblAbstractEvaluationSystem.setBounds(42, 18, 469, 38);
		frame.getContentPane().add(lblAbstractEvaluationSystem);
		
		
		
	}
	
	public void HighLightTextRed(int x, int y, StatDetail stat, JTextArea textArea){
		redPainter = new DefaultHighlighter.DefaultHighlightPainter(new Color(255, 77, 77));
		try {
           // textArea_1.getHighlighter().addHighlight(0, 2, DefaultHighlighter.DefaultPainter);
            textArea.getHighlighter().addHighlight(stat.getNoOfCharactors()+x, stat.getNoOfCharactors()+y, redPainter);
            System.out.println("Coloured");
        } catch (BadLocationException ble) {
        	System.out.println("Error");
        }
	}
	
	public void HighLightTextGreen(int x, int y, StatDetail stat,JTextArea textArea){
		greenPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.green);
		try {
           // textArea_1.getHighlighter().addHighlight(0, 2, DefaultHighlighter.DefaultPainter);
            textArea.getHighlighter().addHighlight(stat.getNoOfCharactors()+x, stat.getNoOfCharactors()+y, greenPainter);
        } catch (BadLocationException ble) {
        	System.out.println("Error");
        }
	}
	
	public void HighLightTextRed(int x, int y,JTextArea textArea){
		redPainter = new DefaultHighlighter.DefaultHighlightPainter(new Color(255, 77, 77));
		try {
           // textArea_1.getHighlighter().addHighlight(0, 2, DefaultHighlighter.DefaultPainter);
            textArea.getHighlighter().addHighlight(x, y, redPainter);
        } catch (BadLocationException ble) {
        	System.out.println("Error");
        }
	}
	
	public void HighLightTextGreen(int x, int y,JTextArea textArea){
		greenPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.green);
		try {
           // textArea_1.getHighlighter().addHighlight(0, 2, DefaultHighlighter.DefaultPainter);
            textArea.getHighlighter().addHighlight(x, y, greenPainter);
        } catch (BadLocationException ble) {
        	System.out.println("Error");
        }
	}
	

	@Override
	public void active() {
		System.out.println("[Live] .... " + super.agent);	
		MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
        //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
        MaSMTMessage m =new MaSMTMessage(agent,cnt,agent, 
        "sendDetails", "b","text", "broadcast");
        sendMessage(m);
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		System.out.println("End gui");
		setLive(false);
	}

	@Override
	public void live() {
		// TODO Auto-generated method stu    
		System.out.println("[Live] .... " + super.agent);
		String sntCnt,rem,grammaErrorCnt,mword,probCount,probSituation="",solCount,methCount,resCount;
 	   	float wordCount=coun,presentageForGrammar = 0,wordRepetPresentage=0,probCountPresent=0,solCountPresent=0,methCountPresent=0,resCountPresent=0;
		tempmes = waitForMessage();
	       if(tempmes.message.equals("NoOfSentences")){
	    	   
	    	   sntCnt=tempmes.content;
	    	   sc=Integer.parseInt(sntCnt);
	    	   rem = tempmes.type;

	    	   
	    	   System.out.println("Sentence count   "+sntCnt);
	    	   System.out.println("Type "+rem);
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent grammar =new MaSMTAbstractAgent("masmt", "grammar",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m1 =new MaSMTMessage(agent,grammar,agent, 
	           "sendGrammarDetails", "b","text", "broadcast");
	           sendMessage(m1);
	           
	           if(Integer.parseInt(rem)<0){
	        	   if(Integer.parseInt(rem)<0 && Integer.parseInt(rem)>=-5){
	        		   marks+=4;
	        		   textArea_1.append("Too much words you have to remove some words\n");
	        	   }else if(Integer.parseInt(rem)<-5 && Integer.parseInt(rem)>=-10){
	        		   marks+=3;
	        		   textArea_1.append("Too much words you have to remove some words\n");
	        	   }else if(Integer.parseInt(rem)<-10 && Integer.parseInt(rem)>=-15){
	        		   marks+=2;
	        		   textArea_1.append("Too much words you have to remove some words\n");
	        	   }else if(Integer.parseInt(rem)<-15 && Integer.parseInt(rem)>=-20){
	        		   marks+=1;
	        		   textArea_1.append("Too much words you have to remove some words\n");
	        	   }else{
	        		   marks+=0;
	        		   textArea_1.append("Too much words you have to remove some words\n");
	        	   }
	           }else if(Integer.parseInt(rem)>0){
	        	   if(Integer.parseInt(rem)>0 && Integer.parseInt(rem)<=5){
	        		   marks+=4;
	        		   textArea_1.append("Less words you have add some words\n");
	        	   }else if(Integer.parseInt(rem)>5 && Integer.parseInt(rem)<=10){
	        		   marks+=3;
	        		   textArea_1.append("Less words you have add some words\n");
	        	   }else if(Integer.parseInt(rem)>10 && Integer.parseInt(rem)<=15){
	        		   marks+=2;
	        		   textArea_1.append("Less words you have add some words\n");
	        	   }else if(Integer.parseInt(rem)>15 && Integer.parseInt(rem)<=20){
	        		   marks+=1;
	        		   textArea_1.append("Less words you have add some words\n");
	        	   }else{
	        		   marks+=0;
	        		   textArea_1.append("Less words you have add some words\n");
	        	   }
	           }else{
	        	   marks+=5;
	        	   textArea_1.append("Word limit is ok\n");
	           }
	           
	           
	       }else if(tempmes.message.equals("GrammarAgent")){
	    	   grammaErrorCnt=tempmes.content;
	    	   System.out.println("Number of errors "+grammaErrorCnt);
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent mwords =new MaSMTAbstractAgent("masmt", "mwords",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m2 =new MaSMTMessage(agent,mwords,agent, 
	           "sendMultipleWordsDetails", "b","text", "broadcast");
	           sendMessage(m2);
	           
	           presentageForGrammar = (Float.parseFloat(grammaErrorCnt)/wordCount)*100;
	           System.out.println("Gr presentage "+presentageForGrammar);
	           
	           if(presentageForGrammar==0.0){
	        	   marks+=10;
	        	   textArea_1.append("Perfect no grammar or spelling errors\n");
	           }else if(presentageForGrammar>0.01 && presentageForGrammar <=0.5){
	        	   marks+=9;
	        	   textArea_1.append("You have to correct the grammar or spelling errors\n");
	           }else if(presentageForGrammar>0.5 && presentageForGrammar <=1.0){
	        	   marks+=8;
	        	   textArea_1.append("You have to correct the grammar or spelling errors\n");
	           }else if(presentageForGrammar>1.0 && presentageForGrammar <=1.5){
	        	   marks+=7;
	        	   textArea_1.append("You have to correct the grammar or spelling errors\n");
	           }else if(presentageForGrammar>1.5 && presentageForGrammar <=2.0){
	        	   marks+=6;
	        	   textArea_1.append("You have to correct the grammar or spelling errors\n");
	           }else if(presentageForGrammar>2.0 && presentageForGrammar <=2.5){
	        	   marks+=5;
	        	   System.out.println("HERE fdgdgf  "+marks);
	        	   textArea_1.append("You have to correct the grammar or spelling errors\n");
	           }else if(presentageForGrammar>2.5 && presentageForGrammar <=3.0){
	        	   marks+=4;
	        	   textArea_1.append("You have lots of grammar or spelling errors correct them to improve abstract\n");
	           }else if(presentageForGrammar>3.0 && presentageForGrammar <=3.5){
	        	   marks+=3;
	        	   textArea_1.append("You have lots of grammar or spelling errors correct them to improve abstract\n");
	           }else if(presentageForGrammar>3.5 && presentageForGrammar <=4.0){
	        	   marks+=2;
	        	   textArea_1.append("You have lots of grammar or spelling errors correct them to improve abstract\n");
	           }else if(presentageForGrammar>4.0 && presentageForGrammar <=4.5){
	        	   marks+=1;
	        	   textArea_1.append("You have lots of grammar or spelling errors correct them to improve abstract\n");
	           }else{
	        	   marks+=0;
	        	   textArea_1.append("You have lots of grammar or spelling errors correct them to improve abstract\n");
	           }
	           
	       }else if(tempmes.message.equals("countOfMultipleWords")){
	    	   mword=tempmes.content;
	    	   System.out.println("Multi words count "+mword);
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent problemCheck =new MaSMTAbstractAgent("masmt", "problemCheck",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m3 =new MaSMTMessage(agent,problemCheck,agent, 
	           "sendProblemRelateDetails", "b","text", "broadcast");
	           sendMessage(m3);
	           
	    	   wordRepetPresentage = (Float.parseFloat(mword)/wordCount)*100;
	    	   System.out.println("Word repet presentage "+wordRepetPresentage);
	    	   
	    	   if(wordRepetPresentage==0.0){
	    		  // marks+=10;
	    		   textArea_1.append("Perfect no repet words\n");
	    	   }else if(wordRepetPresentage>0.01 && wordRepetPresentage<=0.5){
	    		  // marks+=9;
	    		   textArea_1.append("You have to remove the repeating words\n");
	    	   }else if(wordRepetPresentage>0.5 && wordRepetPresentage<=1.0){
	    		  // marks+=8;
	    		   textArea_1.append("You have to remove the repeating words\n");
	    	   }else if(wordRepetPresentage>1.0 && wordRepetPresentage<=1.5){
	    		  // marks+=7;
	    		   textArea_1.append("You have to remove the repeating words\n");
	    	   }else if(wordRepetPresentage>1.5 && wordRepetPresentage<=2.0){
	    		 //  marks+=6;
	    		   textArea_1.append("You have to remove the repeating words\n");
	    	   }else if(wordRepetPresentage>2.0 && wordRepetPresentage<=2.5){
	    		 //  marks+=5;
	    		   textArea_1.append("You have to remove the repeating words\n");
	    	   }else if(wordRepetPresentage>2.5 && wordRepetPresentage<=3.0){
	    		 //  marks+=4;
	    		   textArea_1.append("Word repeating is high reduce them to improve the abstract\n");
	    	   }else if(wordRepetPresentage>3.0 && wordRepetPresentage<=3.5){
	    		 //  marks+=3;
	    		   textArea_1.append("Word repeating is high reduce them to improve the abstract\n");
	    	   }else if(wordRepetPresentage>3.5 && wordRepetPresentage<=4.0){
	    		 //  marks+=2;
	    		   textArea_1.append("Word repeating is high reduce them to improve the abstract\n");
	    	   }else if(wordRepetPresentage>4.0 && wordRepetPresentage<=4.5){
	    		 //  marks+=1;
	    		   textArea_1.append("Word repeating is high reduce them to improve the abstract\n");
	    	   }else{
	    		   marks+=0;
	    		   textArea_1.append("Word repeating is high reduce them to improve the abstract\n");
	    	   }
	    	   
	       }else if(tempmes.message.equals("countOfProblemRelateWords")){
	    	   probCount=tempmes.content;
	    	   probSituation =tempmes.type;
	    	   probCountPresent = (Float.parseFloat(probCount)/sc)*100;
	    	   System.out.println("Prob presentage "+probCountPresent+" "+sc);
	    	   System.out.println("Problem count "+probCount);
	    	   
	    	   if(solHandle==0){
	    		   ++solHandle;
	    		   System.out.println("[Live] .... " + super.agent);	
		   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
		           MaSMTAbstractAgent solutionCheck =new MaSMTAbstractAgent("masmt", "problemCheck",1);
		           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
		           MaSMTMessage m4 =new MaSMTMessage(agent,solutionCheck,agent, 
		           "sendSolutionRelateDetails", "b","text", "broadcast");
		           sendMessage(m4);
	    	   }else{
	    		   System.out.println("gdgfgdgdggdgdfgfggdfgfgdg");
	    	   }
	    	   if(Integer.parseInt(probCount)==0){
	    		   marks+=0;
	    		   textArea_1.append(prob="Problem not defined\n");
	    	   }else if(probCountPresent>0.0 && probCountPresent<=15.0 && probSituation.equals("subjSvo")){    		   
	    		   subj=true;
	    		   ++scnt;
	    		   if(Integer.parseInt(probCount)==scnt && subj){
		    		   textArea_1.append(prob="Research Problem defined well, it's better if you can describe more about the research problem\n");
		    		   marks+=2;
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=1;
		    		   textArea_1.append(prob="Research problem defined, it's better if you can describe it more and describe as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>0.0 && probCountPresent<=15.0 && (probSituation.equals("notInSvo")||probSituation.equals("verbSvo")||probSituation.equals("objSvo")||probSituation.equals("notInSvo")||probSituation.equals("NoSvo"))){
	    		   ++scnt;
	    		   if(Integer.parseInt(probCount)==scnt && subj){
	    			   textArea_1.append(prob="Research Problem defined well, it's better if you can describe more about the research problem\n");
		    		   marks+=2;
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=1;
		    		   textArea_1.append(prob="Research problem defined, it's better if you can describe it more and describe as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>15.0 && probCountPresent<=25.0 && (probSituation.equals("notInSvo")||probSituation.equals("verbSvo")||probSituation.equals("objSvo")||probSituation.equals("notInSvo")||probSituation.equals("NoSvo"))){
	    		   ++scnt;
	    		   if(subj && Integer.parseInt(probCount)==scnt){
		    		   marks+=4;
		    		   textArea_1.append(prob="Research problem defined well, it's better if you can describe little more about the problem\n");
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=3;
		    		   textArea_1.append(prob="Research problem defined, it's better if you can describe it as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>15.0 && probCountPresent<=25.0 && probSituation.equals("subjSvo")){
	    		   subj =true;
	    		   ++scnt;
	    		   if(subj && Integer.parseInt(probCount)==scnt){
		    		   marks+=4;
		    		   textArea_1.append(prob="Research problem defined well, it's better if you can describe little more about the problem\n");
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=3;
		    		   textArea_1.append(prob="Research problem defined, it's better if you can describe it as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>25.0 && probCountPresent<=40.0 && probSituation.equals("subjSvo")){
	    		   ++scnt;
	    		   subj =true;
	    		   if(subj && Integer.parseInt(probCount)==scnt){
	    			   marks+=5;
		    		   textArea_1.append(prob="Problem defined very well\n");
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=4;
		    		   textArea_1.append(prob="Problem defined well, it's better if you can describe it as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>25.0 && probCountPresent<=40.0 && (probSituation.equals("notInSvo")||probSituation.equals("verbSvo")||probSituation.equals("objSvo")||probSituation.equals("notInSvo")||probSituation.equals("NoSvo"))){
	    		   ++scnt;
	    		   if(subj && Integer.parseInt(probCount)==scnt){
	    			   marks+=5;
		    		   textArea_1.append(prob="Problem defined very well\n");
		    	   }else if(Integer.parseInt(probCount)==scnt && subj==false){
		    		   marks+=4;
		    		   textArea_1.append(prob="Problem defined well it's better if you can describe as a subject of a sentence\n");
		    		   System.out.println("No subj ----------------------------");
		    	   }
	    	   }else if(probCountPresent>40.0){
	    		   marks+=2;
	    		   textArea_1.append(prob="Too much described about the problem\n");
	    	   }else{
	    		   textArea_1.append(prob="Error\n");
	    	   }
	       }else if(tempmes.message.equals("countOfSolutionRelateWords")){
	    	   solCount =tempmes.content;
	    	   solCountPresent = (Float.parseFloat(solCount)/sc)*100;
	    	   System.out.println("Solution count "+solCount);
	    	   System.out.println("Sol presentage "+solCountPresent);
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent methodologyCheck =new MaSMTAbstractAgent("masmt", "methodologyCheck",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m5 =new MaSMTMessage(agent,methodologyCheck,agent, 
	           "sendMethodologyRelateDetails", "b","text", "broadcast");
	           sendMessage(m5);
	    	   
	    	   if(solCountPresent==0.0){
	    		   marks+=0;
	    		   textArea_1.append(sol="Solution is not defined\n");
	    	   }else if(solCountPresent>0.0 && solCountPresent<=10.0){
	    		   marks+=1;
	    		   textArea_1.append(sol="Solution is defined, it's better if you can describe more about the sloution\n");
	    	   }else if(solCountPresent>10.0 && solCountPresent<=15.0){
	    		   marks+=2;
	    		   textArea_1.append(sol="Solution is defined, it's better if you can describe more about the sloution\n");
	    	   }else if(solCountPresent>15.0 && solCountPresent<=20.0){
	    		   marks+=3;
	    		   textArea_1.append(sol="Solution is defined, it's better if you can describe more about the sloution\n");
	    	   }else if(solCountPresent>20.0 && solCountPresent<=30.0){
	    		   marks+=4;
	    		   textArea_1.append(sol="Solution is defined well, it's better if you can describe little more about the sloution\n");
	    	   }else if(solCountPresent>30.0 && solCountPresent<=40.0){
	    		   marks+=5;
	    		   textArea_1.append(sol="Solution is defined very well\n");
	    	   }else if(solCountPresent>40.0){
	    		   marks+=2;
	    		   textArea_1.append(sol="too much about solution\n");
	    	   }else{
	    		   textArea_1.append(sol="Erro solution\n");
	    	   }
	       }else if(tempmes.message.equals("countOfMethodologyRelateWords")){
	    	   methCount =tempmes.content;
	    	   methCountPresent = (Float.parseFloat(methCount)/sc)*100;
	    	   System.out.println("Methodology count "+methCount);
	    	   System.out.println("Methodology presentage "+methCountPresent);
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent methodologyCheck =new MaSMTAbstractAgent("masmt", "methodologyCheck",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m6 =new MaSMTMessage(agent,methodologyCheck,agent, 
	           "sendResultRelateDetails", "b","text", "broadcast");
	           sendMessage(m6);
	    	   
	    	   if(methCountPresent==0.0){
	    		   marks+=0;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is not defined\n");
	    	   }else if(methCountPresent>0.0 && methCountPresent<=10.0){
	    		   marks+=1;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is defined, it's better if you can describe more about the methodology\n");
	    	   }else if(methCountPresent>10.0 && methCountPresent<=15.0){
	    		   marks+=2;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is defined, it's better if you can describe more about the methodology\n");
	    	   }else if(methCountPresent>15.0 && methCountPresent<=20.0){
	    		   marks+=3;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is defined, it's better if you can describe more about the methodology\n");
	    	   }else if(methCountPresent>20.0 && methCountPresent<=30.0){
	    		   marks+=4;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is defined well, it's better if you can describe little more about the methodology\n");
	    	   }else if(methCountPresent>30.0 && methCountPresent<=40.0){
	    		   marks+=5;
	    		   counter=90;
	    		   textArea_1.append(meth="Methodology is defined very well\n");
	    	   }else if(methCountPresent>40.0){
	    		   marks+=2;
	    		   counter=90;
	    		   textArea_1.append(meth="too much about Methodology\n");
	    	   }else{
	    		   counter=90;
	    		   textArea_1.append(meth="Erro Methodology\n");
	    	   }
	       }else if(tempmes.message.equals("countOfResultRelateWords")){
	    	   resCount =tempmes.content;
	    	   resCountPresent = (Float.parseFloat(resCount)/sc)*100;
	    	   System.out.println("Result count "+resCount);
	    	   System.out.println("Result presentage "+resCountPresent);
	    	   Runnable runme = new Runnable() {
                   public void run() {
                     progressBar.setValue(counter);
                   }
                 };
               SwingUtilities.invokeLater(runme);
	    	   
	    	   if(resCountPresent==0.0){
	    		   marks+=0;
	    		   textArea_1.append(res="Result is not defined\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>0.0 && resCountPresent<=10.0){
	    		   marks+=1;
	    		   textArea_1.append(res="Result is defined, it's better if you can describe more about the Result\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>10.0 && resCountPresent<=15.0){
	    		   marks+=2;
	    		   textArea_1.append(res="Result is defined, it's better if you can describe more about the Result\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>15.0 && resCountPresent<=20.0){
	    		   marks+=3;
	    		   textArea_1.append(res="Result is defined, it's better if you can describe more about the Result\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>20.0 && resCountPresent<=30.0){
	    		   marks+=4;
	    		   textArea_1.append(res="Result is defined well, it's better if you can describe little more about the Result\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>30.0 && resCountPresent<=40.0){
	    		   marks+=5;
	    		   textArea_1.append(res="Result is defined very well\n");
	    		   Gui.counter=100;
	    	   }else if(resCountPresent>40.0){
	    		   marks+=2;
	    		   textArea_1.append(res="too much about Result\n");
	    		   Gui.counter=100;
	    	   }else{
	    		   textArea_1.append(res="Erro Result\n");
	    		   Gui.counter=100;
	    		   
	    	   }
	       }
	       float k=((marks/40)*100);
	       System.out.println("Marks "+marks);
	       int m =(int)k;
	       labelMarks1.setText(Integer.toString(m));
	       System.out.println("ddasdad are   "+m);
	       
	       if(prob!=null && sol!=null && meth!=null && res!=null){
	    	   GenerateExcel ec =new GenerateExcel();
	    	   ec.excel(prob, sol, meth, res);
	    	   ec.genPdf(prob, sol, meth, res,abstractt,GrammarChecker.errors,MultipleWords.multipleWords,Integer.toString(m),GrammarChecker.ge);
	    	   
	    	   Runnable runme = new Runnable() {
                   public void run() {
                     progressBar.setValue(counter);
                   }
                 };
               SwingUtilities.invokeLater(runme);
              // setLive(false);
               //sma.waitUntilTaskComplete();
	       }
	       
	      
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(mntmWordLimit) || e.getSource().equals(mntmAddValues)) {
			SetWordLimit word = new SetWordLimit(frame);
		}else if(e.getSource().equals(mntmExit)) {
			System.exit(JFrame.EXIT_ON_CLOSE);
		}else if(e.getSource().equals(btnNewButtonsyno)){
			clean();
		}
	}

	private void clean() {
		progressBar.setValue(0);
		lblwords.setText("");
		labelSent.setText("");
		labelAvg.setText("");
		labelMarks1.setText("");
		StatDetail st =new StatDetail();
		st.setAb("");
		textArea.setText("");
		textArea_1.setText("");
		st.count=0;
		st.count1=0;
		coun=0;
		marks=0;
		ProblemChecking.probCnt=0;
		SolutionChecking.solCnt=0;
		MethodologyChecking.methoCnt=0;
		Result.ResCnt=0;
		MultipleWords.moreThan3=0;
		subj = false;
		solHandle=0;
		prob = null;
		sol=null;
		meth=null;
		res=null;
		scnt=0;
		GrammarChecker.errorcnt=0;
	}
}