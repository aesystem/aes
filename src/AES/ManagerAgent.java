package AES;

import masmt2.message.MaSMTMessage;

/**
 *
 * @author Hasaka
 */
public class ManagerAgent extends masmt2.agent.MaSMTManager {
    
    MaSMTMessage tempmes;

    public ManagerAgent() {
        super();
    }

    
    public static void main(String[] args) {
    	 Gui a =new Gui();
         a.gstart();
        
    }
    
    public void createAgentWithRule(){
        
    }

    @Override
    public void active() {
        System.out.println("[Active] MANAGER  .... " + super.agent);
        setNumberofClients(9);
        agents[0] = new Gui("gui",0,"masmt");
        agents[1] = new SetWordLimit("wordCount",1,"masmt");
        agents[2] = new GrammarChecker("Grammar",2,"masmt");
        agents[3] = new MultipleWords("mword",3,"masmt");
        agents[4] = new ProblemChecking("problemCheck",4,"masmt");
        agents[5] = new SVOextracter("svo",5,"masmt");
        agents[6] = new SolutionChecking("solCheck",6,"masmt");
        agents[7] = new MethodologyChecking("methoCheck", 7, "masmt");
        agents[8] = new Result("Result", 8, "masmt");
        
        activeAllClients();
        activeMessageParsing();
       
        
        
    }

    @Override
    public void live() {
        
    }

    @Override
    public void end() {
        
    }
    
}