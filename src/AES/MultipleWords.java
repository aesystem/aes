package AES;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JTextArea;







import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class MultipleWords extends masmt2.agent.MaSMTAgent {
	
	static int moreThan3=0;
	public static List<String> multipleWords;
	MaSMTMessage tempmes;
	
	public MultipleWords(String name,int id,String gp) {
		super(gp,name,id);
	}
	
	public MultipleWords() {
		
	}
	
	public void wordusing(String everything, JTextArea area, StatDetail st){
		List<String> mw = new ArrayList<String>();
		Map<String, Integer> unique = new TreeMap<String, Integer>();
	       // String string1="Ram:Ram: Dog: Dog: Dog: Dog:leela:leela:house:house:shayam";
	        //String string2[]=everything.split(" ");
			int a=0;
	        
	        MaxentTagger tagger = new MaxentTagger("taggers\\english-caseless-left3words-distsim.tagger");
	        
	        everything =everything.trim();
	        everything = everything.replaceAll("([^a-zA-Z\\s])", "");
	        String[] words = everything.split("\\s+");
	        

	        String taggedSample = tagger.tagString(everything);
	        String[] taggedWords = taggedSample.split("\\s+");
	        String [] multiOccurance =new String[words.length];
	        String [] multiTag =new String[words.length];
	        Synonims syno = new Synonims();
	        ProblemChecking check =new ProblemChecking(st);
	        SolutionChecking scheck =new SolutionChecking(st);
	        MethodologyChecking mcheck =new MethodologyChecking(st);
	        Result result =new Result(st);
	        
	        for (int i=0; i<words.length; i++) 

	         {
	            String string=words[i];
	            unique.put(string,(unique.get(string) == null?1:(unique.get(string)+1)));
	            //check.ProblemRelateWords(string);
	            if(unique.get(string)==3){
	            	if("DT".equals(taggedWords[i].substring(words[i].length() + 1)) || "IN".equals(taggedWords[i].substring(words[i].length() + 1))
	            			|| "TO".equals(taggedWords[i].substring(words[i].length() + 1)) || "VBP".equals(taggedWords[i].substring(words[i].length() + 1))
	            			|| "VBZ".equals(taggedWords[i].substring(words[i].length() + 1)) || "PRP".equals(taggedWords[i].substring(words[i].length() + 1))
	            			|| "CC".equals(taggedWords[i].substring(words[i].length() + 1))){
		        		
		        	}else{
		        		++moreThan3;
		        		multiOccurance[a]=words[i];
		        		multiTag[a] = taggedWords[i].substring(words[i].length() + 1);
		        		++a;
		        		//area.append("\n"+words[i]+" = "+unique.get(words[i]));
		        	}
	            }
	          }
	        
	        area.append("\n----------------------------------------------------------------------------\nWords wchich are used three or more");
	        System.out.println("More than three times used "+moreThan3);
        	for(int i=0; i<a;i++){
	        	area.append("\n"+multiOccurance[i]+" = "+unique.get(multiOccurance[i]));
	        	mw.add(multiOccurance[i]+" = "+unique.get(multiOccurance[i]));
	        }
        	area.append("\n\n");
        	multipleWords=mw;
	        /*area.append("\n\nSuggested synonims for words which are used more than twice\n\n ");
	        
	        for(int i=0; i<a;i++){
	        	if(unique.get(multiOccurance[i])>2){
	        		syno.getSynonims(multiOccurance[i], area,multiTag[i]);
	        	}
	        }*/


	       // System.out.println("\n"+unique);       
	}

	@Override
	public void active() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void live() {
		System.out.println("[Live] .... " + super.agent);
		
        
		tempmes = waitForMessage();
	       if(tempmes.message.equals("sendMultipleWordsDetails")){
	    	   
	    	   System.out.println("[Live] .... " + super.agent);	
	   		   //MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	           MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
	           //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	           MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
	           "countOfMultipleWords", Integer.toString(moreThan3),"text", "broadcast");
	           sendMessage(m);
	           
	       }
		
	}
}
