package AES;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ProgressExample
{
    public static JProgressBar progressBar;

    private void createAndDisplayGUI()
    {
        JFrame frame = new JFrame("Progress Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(5, 5));

        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);                

        JButton button = new JButton("START");
        button.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                progressBar.setIndeterminate(true);
                WorkingDialog wd = new WorkingDialog();
                wd.createAndDisplayDialog();
            }
        });

        contentPane.add(progressBar, BorderLayout.PAGE_START);
        contentPane.add(button, BorderLayout.PAGE_END);

        frame.setContentPane(contentPane);
        frame.pack();
        frame.setVisible(true);
    }   

    public static void main(String... args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                new ProgressExample().createAndDisplayGUI();
            }
        });
    }
}

class WorkingDialog extends JDialog
{
    private String message = "HelloWorld";
    private int count = 0;
    private JTextField tfield;
    private Timer timer;

    private ActionListener timerAction = new ActionListener()
    {
        public void actionPerformed(ActionEvent ae)
        {
            if (count == 10)
            {
                timer.stop();
                ProgressExample.progressBar.setIndeterminate(false);
                ProgressExample.progressBar.setValue(100);
                ProgressExample.progressBar.setStringPainted(true);
                dispose();
                return;
            }
            tfield.setText(tfield.getText() + message.charAt(count));
            count++;
        }
    };

    public void createAndDisplayDialog()
    {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLocationByPlatform(true);

        JPanel panel = new JPanel();
        tfield = new JTextField(10);
        
        

        panel.add(tfield);

        add(panel);
        pack();
        setVisible(true);

        timer = new Timer(1000, timerAction);
        timer.start();
    }
}