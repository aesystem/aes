package AES;


import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.trees.*;
import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SVOextracter extends masmt2.agent.MaSMTAgent
{
	  HashMap<String,Integer> subjverb;
	  HashMap<String,Integer> verbobj;
	  HashMap<String,Integer> subjverbobj;
	  
	  public static ArrayList<String> ars;
	  int cnt;
	  int i=0;
	  MaSMTMessage tempmes;
	  
	  public SVOextracter(String name,int id,String gp){
		  super(gp,name,id);
	  }
	  
	  public SVOextracter()
	  {
		  subjverb=new HashMap();
		  verbobj=new HashMap();
		  subjverbobj=new HashMap();
	  }
	  
	  public void setCnt(int ab)
	  {
		  cnt=ab;
	  }
	 
	  
	  public void abs(String ab){
		  LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		  SVOextracter s=new SVOextracter();
		  
		  s.sentenceSplit(lp, ab);
		  //  just for the purpose of viewing output
		  //  responsible for viewing the output of SVO triplets
		  Iterator<String> SVOitr= s.subjverbobj.keySet().iterator();
		  ArrayList<String> lst = new ArrayList<String>();
		  //System.out.println(cnt);
		  while(SVOitr.hasNext())
		  {
//			  arss[i]=SVOitr.next().toString();
			  lst.add(SVOitr.next().toString());
			  ++i;
			 // System.out.println("SVO triplet "+ SVOitr.next().toString());
		  }
		  ars=lst;
	  }
	  
	  public List<String> absList(String ab){
		  List<String> res = new ArrayList<String>();
		  LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
		  SVOextracter s=new SVOextracter();
		  
		  s.sentenceSplit(lp, ab);
		  //  just for the purpose of viewing output
		  //  responsible for viewing the output of SVO triplets
		  Iterator<String> SVOitr= s.subjverbobj.keySet().iterator();
		  String[] arss = new String[1];
		  //System.out.println(cnt);
		  while(SVOitr.hasNext())
		  {
			  res.add(SVOitr.next().toString());
			  ++i;
			 // System.out.println("SVO triplet "+ SVOitr.next().toString());
		  }
		  return res;
	  }
	  
	  
	  public void sentenceSplit(LexicalizedParser lp, String filename) 
	  {
		  TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		  GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		  for (List<HasWord> sentence : new DocumentPreprocessor(new StringReader(filename))) 
		  {
			  Tree parse = lp.apply(sentence);
			  GrammaticalStructure gs = gsf.newGrammaticalStructure(parse); 
			  extractSVO(gs);
		  }
	  	}
	  public void extractSVO(GrammaticalStructure gs)
	  {
		  HashMap<String,String> SV =new HashMap();
		  HashMap<String,String> VO =new HashMap();
		  Morphology m=new Morphology();
		  
//		  System.out.println("Total Dependencies  "+gs.typedDependencies());
//		  System.out.println("Relation  "+gs.typedDependencies().iterator().next());
//		  System.out.println("Governor  "+gs.typedDependencies().iterator().next().gov());
//		  System.out.println("Dependent  "+gs.typedDependencies().iterator().next().dep());
//		  System.out.println("Dependency type  "+gs.typedDependencies().iterator().next().reln());
		  
		  Iterator<TypedDependency> Depitr = gs.typedDependencies().iterator();
		  while (Depitr.hasNext())
		  {
			 TypedDependency Dep = Depitr.next();
			 if(Dep.reln().getShortName().equals("nsubj"))
			 {
				 //we split the dependent and governor because it is of form "skipped-1" and so
				 String[] subject=Dep.dep().toString().split("-");
				 String[] verb=Dep.gov().toString().split("-");
				 //we try to convert the word to lower case and stem the word using the morphology class of Stanford parser
				 String key=m.stem(subject[0].toLowerCase())+"-"+m.stem(verb[0].toLowerCase());
				 /*we use an another hash map for fast finding of SVO triplets by  using two hash maps subject to verb and verb to object.we use these lists to 
				  *identify the SVO triplets by hashing from subject to verb and verb to object*/
				 SV.put(Dep.dep().toString(),Dep.gov().toString());
				 if(subjverb.containsKey(key))
				 {
					 subjverb.put(key,subjverb.get(key)+1);
				 }
				 else
				 {
					 subjverb.put(key,1);
				 }		
				 // responsible for the output of the subjverbs
//				 System.out.println("subjverb "+key);
			 }
			 if(Dep.reln().getShortName().equals("dobj"))
			 {
			 	String[] object=Dep.dep().toString().split("-");
			 	String[] verb=Dep.gov().toString().split("-");
			 	String key=m.stem(verb[0].toLowerCase())+"-"+m.stem(object[0].toLowerCase());
			 	VO.put(Dep.gov().toString(),Dep.dep().toString());
				 if(verbobj.containsKey(key))
				 {
					 verbobj.put(key,verbobj.get(key)+1);
				 }
				 else
				 {
					 verbobj.put(key,1);
				 }
				 // responsible for the output for verbobj
//				 System.out.println("verbonj "+key);
			 }
			 
		  }
		  Iterator<String> SVitr=SV.keySet().iterator();
		  while(SVitr.hasNext())
		  {
			  String s=SVitr.next();
			  //from the subject to verb hash map we try to get the verb of the subject
			  String v=SV.get(s);
			  // we match the verb to the verb to object hashmap.to find if there exist any case with same verb
			  if (VO.containsKey(v))
			  {
				  // we need to split because the subjects and verbs have their word order number still to it "helping-5" and so on
				  String[] sparts=s.split("-");
				  String[] vparts=v.split("-");
				  String subject=m.stem(sparts[0].toLowerCase());
				  String verb=m.stem(vparts[0].toLowerCase());
				  if(subjverb.get(subject+"-"+verb)-1==0)
				  {
				  subjverb.remove(subject+"-"+verb) ;
				  }
				  else
				  {
				  subjverb.put(subject+"-"+verb,subjverb.get(subject+"-"+verb)-1);
				  }
				  String[] oparts=VO.get(v).split("-");
				  String object=m.stem(oparts[0].toLowerCase());
				  if(verbobj.get(verb+"-"+object)-1==0)
				  {
					  verbobj.remove(verb+"-"+object) ;
				  }
				  else
				  {
					  verbobj.put(verb+"-"+object,verbobj.get(verb+"-"+object)-1);
				  }
				  String SVOkey=subject+"-"+verb+"-"+object;
				  if(subjverbobj.containsKey(SVOkey))
				  {
					  subjverbobj.put(SVOkey,subjverbobj.get(SVOkey)+1);
				  }
				  else
				  {
					  subjverbobj.put(SVOkey,1);
				  }
			  }
		  }
	}

	@Override
	public void active() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void live() {
		System.out.println("[Live] .... " + super.agent);
		SVOextracter ab = new SVOextracter();
        
		tempmes = waitForMessage();
	       if(tempmes.message.equals("ProblemRelateSentence")){
	    	   String sentence=tempmes.content;
	    	   String word =tempmes.type;
	    	   String [] svoWords = new String[5];
	    	   ab.abs(sentence);
	    	   if(ars.size() != 0){
//	    	   try {
				   for (String obj : ars) {
					   obj = obj.replaceAll("-", " ");
					   obj = obj.replaceAll("([^a-zA-Z\\s])", " ");
					   svoWords = obj.split(" ");// check word count stat detail

					   if (svoWords[0].equals(word)) {
						   System.out.println("Problem written as subject");

						   System.out.println("[Live] .... " + super.agent);
						   MaSMTAbstractAgent Gui = new MaSMTAbstractAgent("masmt", "Gui", 1);
						   MaSMTMessage m1 = new MaSMTMessage(agent, Gui, agent,
								   "ProblemSituation", "subjSvo", "text", "broadcast");
						   sendMessage(m1);

					   } else if (svoWords[2].equals(word)) {
						   System.out.println("Problem written as verb");

						   System.out.println("[Live] .... " + super.agent);
						   MaSMTAbstractAgent Gui = new MaSMTAbstractAgent("masmt", "Gui", 1);
						   MaSMTMessage m1 = new MaSMTMessage(agent, Gui, agent,
								   "ProblemSituation", "verbSvo", "text", "broadcast");
						   sendMessage(m1);

					   } else if (svoWords[4].equals(word)) {
						   System.out.println("Problem written as object");

						   System.out.println("[Live] .... " + super.agent);
						   MaSMTAbstractAgent Gui = new MaSMTAbstractAgent("masmt", "Gui", 1);
						   MaSMTMessage m1 = new MaSMTMessage(agent, Gui, agent,
								   "ProblemSituation", "objSvo", "text", "broadcast");
						   sendMessage(m1);

					   } else {
						   System.out.println("Problem is not in SVO relation");

						   System.out.println("[Live] .... " + super.agent);
						   MaSMTAbstractAgent Gui = new MaSMTAbstractAgent("masmt", "Gui", 1);
						   MaSMTMessage m1 = new MaSMTMessage(agent, Gui, agent,
								   "ProblemSituation", "notInSvo", "text", "broadcast");
						   sendMessage(m1);

					   }
				   }


//	    		   ars[0]=ars[0].replaceAll("-", " ");
//	    		   ars[0]=ars[0].replaceAll("([^a-zA-Z\\s])", " ");
//	    		   svoWords = ars[0].split(" ");
			   }else{
				   System.out.println("This sentence dosen't have SVO relation");

	    		   System.out.println("[Live] .... " + super.agent);
     	           MaSMTAbstractAgent Gui =new MaSMTAbstractAgent("masmt", "Gui",1);
     	           MaSMTMessage m1 =new MaSMTMessage(agent,Gui,agent,
     	           "ProblemSituation", "NoSvo","text", "broadcast");
     	           sendMessage(m1);
			   }
//	    	   }catch(Exception e){
//	    		   System.out.println("This sentence dosen't have SVO relation");
//
//	    		   System.out.println("[Live] .... " + super.agent);
//     	           MaSMTAbstractAgent Gui =new MaSMTAbstractAgent("masmt", "Gui",1);
//     	           MaSMTMessage m1 =new MaSMTMessage(agent,Gui,agent,
//     	           "ProblemSituation", "NoSvo","text", "broadcast");
//     	           sendMessage(m1);
//	    	   }
	       }
	}
}