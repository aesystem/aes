package AES;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import masmt2.agent.MaSMTAbstractAgent;
import masmt2.message.MaSMTMessage;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.JDOMParseException;
import org.jdom2.input.SAXBuilder;


public class SolutionChecking extends masmt2.agent.MaSMTAgent {
	String msg;
	static int solCnt=0;
	static StatDetail st;
	MaSMTMessage tempmes;
	
	public SolutionChecking(String name,int id,String gp) {
		super(gp,name,id);
	}
	
	public SolutionChecking(StatDetail st) {
		SolutionChecking.st=st;
	}

	@Override
	public void active() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void live() {
		System.out.println("[Live] .... " + super.agent);
		List<String> morpoWords;
       	morp mo =new morp();
        
		tempmes = waitForMessage();
	       if(tempmes.message.equals("sendSolutionRelateDetails")){
	    	String[] words1 = st.WordFrmSentense();
	   		for(int i=0; i<st.count1; i++){
	           	//String wrd = words1[i];
	           	String parts[] = words1[i].split(" ");
	           	int len = parts.length;
	           	
	           	try {
	                   //GET XML FILE NAME
	                   Document doc;
	                   Element root;
	                   List<Element> elements;
	                   // Information about English morphologycal rules
	           
	                   String xmlFileName = "solution.xml";
	                //   System.out.println(">>" +xmlFileName);
	                   File xmlFile = new File(xmlFileName);
	                   if (xmlFile.exists()) 
	                   {
	                       FileInputStream fis = new FileInputStream(xmlFile);
	                       SAXBuilder sb = new SAXBuilder();
	                       doc = sb.build(fis);
	                       root = doc.getRootElement();
	                       fis.close();
	                       elements = root.getChildren("agent");
	                       //staff.getAttribute("id").setValue("2");
	                       Element tmpNode;
	                       String name;
	                       
	                       for (int j = 0; j < elements.size(); j++) {

	                           tmpNode = elements.get(j);                 
	                           name = tmpNode.getAttributeValue("name");
	                           for(int k=0; k<len;k++){
	                        	    String word = parts[k];
		                        	morpoWords=mo.morphology(word);
		                           	for(String obj: morpoWords){
		                           		if(name.equals(obj)){
			                           		System.out.println(word+" solution is in "+i+" sentense");
			                           		/*svo.abs(words1[i]);
			                           		System.out.println("SVO relation solution "+svo.ars[0]);*/
			                           		++solCnt;
			                           	}
	                    		}
	                           }
	                           //System.out.println(name+" "+qun); 
	                           
	                       }
	                       //System.out.println("Problem count "+solCnt);
	                       

	                   } 
	                   else 
	                   {
	                       System.out.println("ERR:...");
	                       // File Not Avialable
	                   }
	               } catch (IOException io) {
	                   System.out.println("XML_ERR" + io.getMessage());
	               } catch (JDOMParseException io) {
	                   System.out.println("XML_ERR" + io.getMessage());
	               } catch (JDOMException e) {
	                   System.out.println("XML_ERR" + e.getMessage());
	               }
	           }
	   		System.out.println("[Live] .... " + super.agent);	
	   		//MaSMTAbstractAgent cnt =new MaSMTAbstractAgent("masmt", "WordCount",1);
	        MaSMTAbstractAgent gui =new MaSMTAbstractAgent("masmt", "gui",1);
	        //MaSMTAbstractAgent gs =new MaSMTAbstractAgent("masmt", "goat",3);
	        MaSMTMessage m =new MaSMTMessage(agent,gui,agent, 
	        "countOfSolutionRelateWords", Integer.toString(solCnt),"text", "broadcast");
	        sendMessage(m);
	   		
	       }
		
	}
}
